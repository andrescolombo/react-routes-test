import React, {useState, useEffect} from 'react'
import {useParams} from 'react-router-dom'
import axios from 'axios'


const Usuario = () => {

    const [usuario,setUsuario] = useState([])
    const {id} = useParams()

    const obtenerUsuario = async () => {
        // Await espera hasta recibir todos los datos del axios.get
        const res = await axios.get(`http://jsonplaceholder.typicode.com/users/${id}`)
        const users = await res.data
        setUsuario(users)
    }

    //Se pasa un array vacio [] para que no se genere un loop infinito.
    useEffect( () => {
        obtenerUsuario()
    },[])


    return (
        <div>
            <h4>Usuario</h4>
            <p>Nombre : {usuario.name}</p>
            <p>Email : {usuario.email}</p>
            <small>Telefono : {usuario.phone}</small>
        </div>
    )
}

export default Usuario