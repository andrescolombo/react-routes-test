import React, {useState, useEffect} from 'react'
import {Link} from 'react-router-dom'
import axios from 'axios'



const Usuarios = () => {
    
    const [usuarios,setUsuarios] = useState([])

    const obtenerUsuarios = async () => {
        // Await espera hasta recibir todos los datos del axios.get
        const res = await axios.get('http://jsonplaceholder.typicode.com/users')
        const users = await res.data
        setUsuarios(users)
    }

    //Se pasa un array vacio [] para que no se genere un loop infinito.
    useEffect( () => {
        obtenerUsuarios()
    },[])

    return (
        <div>
            <h2>Lista de usuarios</h2>
            {
                usuarios.map( (item) => (
                    <div>
                        <Link to={`/usuario/${item.id}`}key={item.id}>{item.name}</Link>
                    </div>
                )
                ) 
            }
        </div>
    )
}

export default Usuarios
