import React from 'react'
import {useParams} from 'react-router-dom'

const Inicio = () => {

    //useParams toma el valor de la ruta, y con object destructuring se usa ese valor para completar nombre
    const {id, nombre, edad} = useParams()

    return (
        <div>
            <h1>Pagina de inicio</h1>
            {id}/
            {nombre}/
            {edad}/
        </div>
    )
}

export default Inicio
