import React, {useState, useEffect} from 'react'


const Base = () => {

    const [nombre, setNombre] = useState('Pedro')
    
    //UseEffect se ejecuta automaticamente, se llama cuando se termina de visulizar todo el DOM
    useEffect( () => {
        setTimeout( () => {
            setNombre('Andres')
        },2000)
    })

    return (
        <div>
            <h1>Pagina de base</h1>
            {nombre}
        </div>
    )
}

export default Base
