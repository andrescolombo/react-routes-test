import React from 'react'
import Inicio from './components/Inicio'
import Base from './components/Base'
import{
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
} from 'react-router-dom'
import Usuarios from './components/Usuarios'
import Usuario from './components/Usuario'


const App = () => {
  // Crear rutas para la app, por ejemplo pagina.com/inicio en este. Tambien se puede hacer que dependiendo del path cargue un componente u otro.
  return (
    <Router>
      <Link to='/inicio'>Inicio</Link>
      <Link to='/'>Base</Link>


      <Switch>
        {/* con el /:nombre indico que lo que venga en la ruta lo asigno a la variable nombre */}
        <Route exact path='/inicio/:id/:nombre/:edad'>
          <Inicio/>
        </Route>
        {/* Trata de marchear cualquier cosa, por eso necesita el exact */}
        <Route exact path='/'>
          <Base/>
        </Route>
      </Switch>

    <Link to='/'>Usuarios</Link>

    <Switch>
      <Route exact path='/'>
        <Usuarios/>
      </Route>
      <Route path='/usuario/:id'>
        <Usuario/>
      </Route>
    </Switch>

    </Router>

  )
}

export default App

